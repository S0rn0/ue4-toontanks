// Copyright © 2021 Derek Fletcher, All rights reserved

#include "PawnBase.h"

#include "../Actors/ProjectileBase.h"
#include "../Components/HealthComponent.h"

#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APawnBase::APawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

  CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Collider"));
  RootComponent = CapsuleComp;

  BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
  BaseMesh->SetupAttachment(RootComponent);

  TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Mesh"));
  TurretMesh->SetupAttachment(BaseMesh);

  ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
  ProjectileSpawnPoint->SetupAttachment(TurretMesh);

  Health = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
}

void APawnBase::RotateTurret(FVector LookAtTarget) 
{
  const FVector LookAtTargetCleaned = FVector(LookAtTarget.X, LookAtTarget.Y, TurretMesh->GetComponentLocation().Z);
  const FVector StartLocation = TurretMesh->GetComponentLocation();

  TurretMesh->SetWorldRotation((LookAtTargetCleaned - StartLocation).Rotation());
}

void APawnBase::Fire() 
{
  if (ProjectileClass)
  {
    AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(ProjectileClass, ProjectileSpawnPoint->GetComponentLocation(), ProjectileSpawnPoint->GetComponentRotation());
    TempProjectile->SetOwner(this);
  }
}

void APawnBase::HandleDestruction() 
{
  UGameplayStatics::SpawnEmitterAtLocation(this, DeathParticle, GetActorLocation());
  UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
  GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(DeathShake);
}
