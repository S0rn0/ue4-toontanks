// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnBase.generated.h"

class AProjectileBase;
class UCapsuleComponent;
class UHealthComponent;

UCLASS()
class TOONTANKS_API APawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnBase();

  virtual void HandleDestruction();

protected:

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
  UCapsuleComponent* CapsuleComp;
  UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Components)
  UStaticMeshComponent* BaseMesh;
  UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Components)
  UStaticMeshComponent* TurretMesh;
  UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Components)
  USceneComponent* ProjectileSpawnPoint;
  UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = "Projectile Type")
  TSubclassOf<AProjectileBase> ProjectileClass;
  UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = Health)
  UHealthComponent* Health;
  UPROPERTY(EditDefaultsOnly, Category = Effects)
  UParticleSystem* DeathParticle;
  UPROPERTY(EditDefaultsOnly, Category = Effects)
  USoundBase* DeathSound;
  UPROPERTY(EditDefaultsOnly, Category = Effects)
  TSubclassOf<UCameraShakeBase> DeathShake;

  void RotateTurret(FVector LookAtTarget);
  void Fire();

};
