// Copyright © 2021 Derek Fletcher, All rights reserved

#include "PawnTurret.h"

#include "PawnTank.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
APawnTurret::APawnTurret()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void APawnTurret::BeginPlay()
{
	Super::BeginPlay();

  PlayerPawn = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
  GetWorldTimerManager().SetTimer(FireRateTimerHandle, this, &APawnTurret::CheckFireCondition, FireRate, true);
}

void APawnTurret::HandleDestruction() 
{
  Super::HandleDestruction();
  Destroy();
}

// Called every frame
void APawnTurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

  if (PlayerPawn && DistanceToPlayer() <= FireRange)
  {
    RotateTurret(PlayerPawn->GetActorLocation());
  }
}

void APawnTurret::CheckFireCondition()
{
  if (PlayerPawn && PlayerPawn->IsPlayerAlive() && DistanceToPlayer() <= FireRange)
  {
    Fire();
  }
}

float APawnTurret::DistanceToPlayer() 
{
  if (!PlayerPawn)
  {
    return 0.f;
  }

  return FVector::Dist(PlayerPawn->GetActorLocation(), GetActorLocation());
}