// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"
#include "PawnTank.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class TOONTANKS_API APawnTank : public APawnBase
{
	GENERATED_BODY()
	
public:
	// Sets default values for this pawn's properties
	APawnTank();

  // Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

  virtual void HandleDestruction() override;

  bool IsPlayerAlive() const;

protected:

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
  USpringArmComponent* SpringArm;
  UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = Components)
  UCameraComponent* Camera;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
  float MoveSpeed = 128.f;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Movement)
  float RotateSpeed = 128.f;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

  bool bIsPlayerAlive = true;

  FVector MoveDirection;
  FQuat RotationDirection;

  APlayerController* PlayerControllerRef;

  void CalculateMoveInput(float Value);
  void CalculateRotateInput(float Value);
  
  void ApplyMovement();
  void ApplyRotation();

};
