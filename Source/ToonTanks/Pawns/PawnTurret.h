// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "PawnBase.h"
#include "PawnTurret.generated.h"

class APawnTank;

UCLASS()
class TOONTANKS_API APawnTurret : public APawnBase
{
	GENERATED_BODY()
	
public:
	// Sets default values for this pawn's properties
	APawnTurret();

  // Called every frame
	virtual void Tick(float DeltaTime) override;

  virtual void HandleDestruction() override;

protected:

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Combat)
  float FireRate = 2.f;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Combat)
  float FireRange = 512.f;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

  FTimerHandle FireRateTimerHandle;

  APawnTank* PlayerPawn;

  void CheckFireCondition();
  float DistanceToPlayer();

};
