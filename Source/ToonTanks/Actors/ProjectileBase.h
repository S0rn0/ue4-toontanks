// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "ProjectileBase.generated.h"

class UProjectileMovementComponent;

UCLASS()
class TOONTANKS_API AProjectileBase : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileBase();

protected:

  virtual void BeginPlay() override;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
  UProjectileMovementComponent* ProjectileMovement;
  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
  UParticleSystemComponent* ParticleTrail;

  UPROPERTY(EditDefaultsOnly, Category = Effects)
  UParticleSystem* HitParticle;

  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
  TSubclassOf<UDamageType> DamageType;

  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
  float Damage = 64.f;

  UPROPERTY(EditDefaultsOnly, Category = Effects)
  USoundBase* HitSound;
  UPROPERTY(EditDefaultsOnly, Category = Effects)
  USoundBase* LaunchSound;

  UPROPERTY(EditDefaultsOnly, Category = Effects)
  TSubclassOf<UCameraShakeBase> HitShake;

private:

  UFUNCTION()
  void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};
