// Copyright © 2021 Derek Fletcher, All rights reserved

#include "ProjectileBase.h"

#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
  ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));

  ParticleTrail = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Trail"));
  ParticleTrail->SetupAttachment(RootComponent);

  GetStaticMeshComponent()->OnComponentHit.AddDynamic(this, &AProjectileBase::OnHit);
}

void AProjectileBase::BeginPlay() 
{
  Super::BeginPlay();

  UGameplayStatics::PlaySoundAtLocation(this, LaunchSound, GetActorLocation());
}

void AProjectileBase::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) 
{
  AActor* MyOwner = GetOwner();
  if (!MyOwner)
  {
    return;
  }

  if (OtherActor && OtherActor != this && OtherActor != MyOwner)
  {
    UGameplayStatics::ApplyDamage(OtherActor, Damage, MyOwner->GetInstigatorController(), this, DamageType);
    UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, GetActorLocation());
    UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());
    GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(HitShake);
    Destroy();
  }
}
