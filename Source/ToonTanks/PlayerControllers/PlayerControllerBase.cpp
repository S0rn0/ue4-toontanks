// Copyright © 2021 Derek Fletcher, All rights reserved

#include "PlayerControllerBase.h"

void APlayerControllerBase::SetPlayerEnabledState(bool bIsPlayerEnabled) 
{
  if (bIsPlayerEnabled)
  {
    GetPawn()->EnableInput(this);
  }
  else
  {
    GetPawn()->DisableInput(this);
  }

  SetShowMouseCursor(bIsPlayerEnabled);
}
