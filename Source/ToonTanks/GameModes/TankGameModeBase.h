// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TankGameModeBase.generated.h"

class APawnTank;
class APawnTurret;
class APlayerControllerBase;

UCLASS()
class TOONTANKS_API ATankGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

  void ActorDied(AActor* DeadActor);

protected:

  virtual void BeginPlay() override;

  UFUNCTION(BlueprintImplementableEvent)
  void GameStart();
  UFUNCTION(BlueprintImplementableEvent)
  void GameOver(bool bDidPlayerWin);

  UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Game Loop")
  int32 StartDelay = 0.f;

private:

  APawnTank* PlayerTank;
  APlayerControllerBase* PlayerControllerRef;

  uint8 TargetTurrets = 0;

  void HandleGameStart();
  void HandleGameOver(bool bDidPlayerWin);
};
