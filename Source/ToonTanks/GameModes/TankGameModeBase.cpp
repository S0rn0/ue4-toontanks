// Copyright © 2021 Derek Fletcher, All rights reserved

#include "TankGameModeBase.h"

#include "../PlayerControllers/PlayerControllerBase.h"

#include "../Pawns/PawnTank.h"
#include "../Pawns/PawnTurret.h"

#include "Kismet/GameplayStatics.h"

void ATankGameModeBase::BeginPlay() 
{
  Super::BeginPlay();
  HandleGameStart();
}

void ATankGameModeBase::ActorDied(AActor* DeadActor) 
{
  if (DeadActor == PlayerTank)
  {
    PlayerTank->HandleDestruction();
    HandleGameOver(false);

    PlayerControllerRef->SetPlayerEnabledState(false);
  }
  else if (APawnTurret* DestroyedTurret = Cast<APawnTurret>(DeadActor))
  {
    DestroyedTurret->HandleDestruction();
    if (--TargetTurrets == 0) HandleGameOver(true);
  }
}

void ATankGameModeBase::HandleGameStart() 
{
  TArray<AActor*> Turrets;
  UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTurret::StaticClass(), OUT Turrets);
  TargetTurrets = Turrets.Num();

  PlayerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
  PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));

  GameStart();

  PlayerControllerRef->SetPlayerEnabledState(false);

  FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(PlayerControllerRef, &APlayerControllerBase::SetPlayerEnabledState, true);
  FTimerHandle PlayerEnableHandle;

  GetWorld()->GetTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, StartDelay, false);
}

void ATankGameModeBase::HandleGameOver(bool bDidPlayerWin) 
{
  GameOver(bDidPlayerWin);
}
