// Copyright © 2021 Derek Fletcher, All rights reserved

#include "HealthComponent.h"

#include "../GameModes/TankGameModeBase.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
  Health = MaxHealth;

  GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::ApplyDamage);
}

void UHealthComponent::ApplyDamage(AActor* DamageActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser) 
{
  // Check for just zero damage in case we want to be able to add health and heal later
  if (Damage == 0 || Health <= 0) return;

  Health = FMath::Clamp(Health - Damage, 0.f, MaxHealth);

  if (Health <= 0.f) {
    ATankGameModeBase* gm = GetWorld()->GetAuthGameMode<ATankGameModeBase>();
    gm->ActorDied(GetOwner());
  }
}
