// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

class ATankGameModeBase;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOONTANKS_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

protected:

  UPROPERTY(EditDefaultsOnly)
  float MaxHealth = 100.f;

  // Called when the game starts
	virtual void BeginPlay() override;

  UFUNCTION()
  void ApplyDamage(AActor* DamageActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

private:

  float Health = 0.f;

};
